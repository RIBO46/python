import paramiko


def main():
   # ssh_client = paramiko.SSHClient() #в модуле парамико объевляем объект 

   # ssh_client.close()
  # private_key = paramiko.RSAKey.from_private_key("/Users/20111720/.ssh/id_rsa") # для подключения не по паролю, а по ключу 

   with paramiko.SSHClient() as ssh_client:

       ssh_client.load_system_host_keys("/Users/20111720/.ssh/known_hosts") # читаем файл известных хосов (в целом хост не обязательно указывать, она будет искать этот файл в стандартной директори ssh)
       ssh_client.set_missing_host_key_policy(paramiko.WarningPolicy) # если нет файла известных хостов эта политика выводит ошибку но все таки разрешает подключение на данный хост и еще есть AutoAddPolicy (автоэдд добавит этот хост в список авторизованых и ошибок больше не будет)
       ssh_client.connect(hostname="10.255.16.66", port=22, username="avmoskalev", password="123!") # тут передаем 4 обязательных парраметра 
       #ssh_client.connect(hostname="10.255.16.66", port=22, username="avmoskalev", password="123!", pkey=private_key) # тут передаем 4 обязательных парраметра 
       ssh_client.invoke_shell(width=150,
        height=124) # инвок шел нужен для терминала, но эта фигня не нужна  если нам просто нужно выполнять команды
       stdin, stdout, stderr = ssh_client.exec_command("ls /") # 
       ssh_client.exec_command("touch 123.txt")

       #print(stdout.read().decode("utf-8"))

if __name__ == "__main__":
    main() 