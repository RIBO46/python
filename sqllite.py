#sqlite3 IMYADB.db - подключиться к sqlite
###### МЕТАКОМАНДЫ #######
# .tables - показывает доступные таблицы
# .exit - выйти 
# . help - подсказка со списком всех метакоманд
# .schema - посмотреть что у нас за таблицы


# CREATE table switch (
#       mac         text not NULL primary key,
#       hostname    text,
#       mode        text,
#       location    text
# );
#
# DROP TABLE switch - удалить таблицу 
#
# INSERT into switch (mac, mode, hostname, location) values ('123.123.123.123', 'new', 'localhost', 'RU'); - вставка данных
#
# SELECT * FROM switch; - показать все, что есть в таблице 
# SELECT hostname, location from switch; - показывать только определенный столбец
# SELECT * from switch WHERE hostname = 'localhost'; - выбрать все записи где хостнейм равен локалхосту
