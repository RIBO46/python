class Human(): # Объявление класса
    """
    Существо человек  (ОБЪЯВЛЕНИЕ ЧЕРЕЗ ДОТ СТРИНГ, ЧТО НАШ КЛАСС ОЗНАЧАЕТ)
    """

    specialization = "K8S GRANDMASTER"    #члены класса а не объекта и методы класса а не объекта

    @classmethod
    def about_specialization (cls):
        print (cls.specialization)

    def __init__(self, name: str, profession: str, experience: int = 1): # Объявление метода (так же есть магический метод __init__ который инициализирует объект(экземпляр класса) при его создании)
        self.__name = name # присваиваем в приватную переменную имя которые мы передали через метод  # Объявляем конуструктор, а так же 
        self.profession = profession                                                                 # объявляем членов класса 
        self.__experience = experience                                                               # _переменная (защищеная переменая (на уровне соглашений), __переменная(приватная переменая можно обращаться только внутри класса))

    def GetHuman(self): # это гетер для возрата или передачи переменых
        print (f"Привет! Меня звоут {self.__name}, я работаю {self.profession} и мой стаж {self.__experience} лет")
        return self.__name

    def ChangeHuman(self, new_vaule: int, reason: str): # это сетер
        self.__experience = new_vaule
        self.reason = reason
        
    def ChangeReason(self) -> dict: 
        self.ChangeHuman(self.__experience, "не просто так") # вызов метода внутри класса через вызов экземпляра класса

def main():
    s1 = Human('Вася', 'DevOps', 5)
    s2 = Human('Петя', 'Программист', 15)
    print(s1.specialization)        #
    s2.specialization = "JAVA"      #
    print(s2.specialization)        #
    Human.specialization = "AWS"    # Это глобальная переменна класса, которая присваивается изначально ко всем экземплярам
    print(s2.specialization)        # Тут будет выводиться JAVA так как разный скоуп(пространство) видимости ( у объектов оно свое нежели у всего класса)
    Human.about_specialization()    # А это глобальный метод класса
    s1.about_specialization()       #
    s2.specialization2 = "PYTHON"   # Создание новой переменной ( но она уже будет относиться только к объекту s2)
    print(s2.specialization2)       #
    s1.GetHuman()
    s2.GetHuman()
    s1.ChangeHuman(1, 'просто так')
    s1.GetHuman()
    s1.profession = "Тестировщиком" # так как переменная не приватная, то можем ее изменять
    s1.GetHuman()
    print(s1.reason)
    s1.ChangeReason()
    print(s1.reason)

if __name__ == '__main__':
    main()