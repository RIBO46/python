import requests
import json

service_name = 'some-test-vm-01'
project_id = 'd1e956ea-3e04-450c-8eb0-bfe2a9886f21'
group_id = '9ea20bbc-457c-4282-8ada-7ae7a09a95de'
api = 'https://portal.gos.sbercloud.dev/api/v1'
token = 'eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI4NGE0OGQ1ZC03ODU5LTRiNzktOWVkYi01MDkyZTRmZjQ5ZmUifQ.eyJqdGkiOiJmZmYxZmEzMy01NDFiLTQ5MjEtYmYxZS04OTFiZTkwMzY3NzQiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNjY5Mjg4MzMwLCJpc3MiOiJodHRwczovL2F1dGguZ29zLnNiZXJjbG91ZC5kZXYvYXV0aC9yZWFsbXMvUGxhdGZvcm1BdXRoIiwiYXVkIjoiaHR0cHM6Ly9hdXRoLmdvcy5zYmVyY2xvdWQuZGV2L2F1dGgvcmVhbG1zL1BsYXRmb3JtQXV0aCIsInN1YiI6IjE3Yjg5Y2U0LTRhZDEtNGQxMi1hOTI2LTk3OGEwOTVlNTA1MSIsInR5cCI6Ik9mZmxpbmUiLCJhenAiOiJQbGF0Zm9ybUF1dGgtUHJveHkiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiIwOWRkZjc1YS0yMTBkLTQyZGQtODYxZS1jOGVlNDY0MjJmODQiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsicGxhdGZvcm1hdXRoX3VzZXIiLCJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJzY29wZSI6Im9mZmxpbmVfYWNjZXNzIGVtYWlsIGVtcGxveWVlIGxvZ2luIn0.6FajNTsM7qFZTGVoPOGCzmR7Qx8l2pfau6MljBE48cM'

class Server:
    uuid = None
    service_name = None
    project_id = None
    group_id = None
    user = None
    password = None
    ip_address = None
    name = None
    flavor = None
    os_name = None
    os_version = None
    storage_type = None
    disk = None
    #storage_type = None
    #hdd = {"size":50, storage_type: "DEFAULt"}
    
    state = None
    volumes = None
    public_ssh = None
    virtualization = "openstack"
    ir_group = "vm"

    def __init__(self, service_name) -> object:
        self.service_name = service_name
        self.storage_type = "iscsi_slow"
        self.disk = 50
        #self.storage_type = "DEFAULT"
        self.virtualization = "openstack"
        self.ir_group = "vm"

server = Server(service_name)
server.flavor = 'm1.tiny'
server.disk = 50
server.os_name = 'altlinux'
server.os_version = '8.2'
server.project_id = project_id
server.group_id = group_id

def instance_create(server: Server, api: str, token: str) -> requests.Response:
    req = requests.session()
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "Authorization": token,
    }
    data = '{ "server": { "service_name": "testmoskalev", "project_id": "d1e956ea-3e04-450c-8eb0-bfe2a9886f21", "group_id": "9ea20bbc-457c-4282-8ada-7ae7a09a95de", "virtualization": "openstack", "flavor": "m1.tiny", "ir_group": "vm", "disk": 50, "os_name": "rhel", "os_version": "7.9", "fault_tolerance": "Stand-alone", "hdd": { "size": 50 }, "volumes": [ { "name": "disk1", "size": 50, "storage_type": "__DEFAULT__" } ] }, "count": 1 }'

    resp = req.post('https://portal.gos.sbercloud.dev/api/v1/servers?api_key=autorize', headers=headers, data=data)

    print(resp.status_code)
    print(resp.text)
    print(resp.json())

create = instance_create(server, api, token)

