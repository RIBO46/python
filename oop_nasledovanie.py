from re import L


class Human():
    zxc = 1
    def print1(self):
        print ('HELLO')


class Programist(Human): # В скобках идет наследование класса и теперь мы можем обращаться к переменым и методам другого класса
    def write_programm(self, code_row_count):
        print(f"пишу программу {code_row_count} на питоне {Human.zxc}")
        self.print1()


def main():
    s = Programist()
    s.write_programm(1)

if __name__ == "__main__":
    main()