import psycopg2


def main():

    conn = psycopg2.connect(
    host="localhost",
    database="person",
    user="postgres",
    password="12345678",
    port="6432") # создаем новую сессию подключения

    cur = conn.cursor() # используя курсор можно исполнять комманды на стороне БД
    print(cur.execute('SELECT version()'))
    cur.execute("DROP TABLE IF EXISTS superheroes")
    cur.execute("DROP TABLE IF EXISTS trafic")


    cur.execute("CREATE TABLE superheroes (hero_id serial PRIMARY KEY, hero_name varchar, strength int);")
    cur.execute("INSERT INTO superheroes (hero_name, strength) VALUES (%s, %s)", ("Supermam", 100)) # %s - это плейсхолдеры для передачи данных в постгрю (format)
    cur.execute("INSERT INTO superheroes (hero_name, strength) VALUES (%s, %s)", ("Flash", 999))
    cur.execute("INSERT INTO superheroes (hero_name, strength) VALUES (%s, %s)", ("Batman", 10))


    conn.commit() # комит не закрывает соединение (просто нашии команды исполнятся на стороне БД)

    cur.execute("CREATE TABLE trafic (trafic serial PRIMARY KEY, light  text);")
    cur.execute("INSERT INTO trafic (light) VALUES (%s)", ("green", )) # надо оставлять запятую после значения

    conn.commit() # комит не закрывает соединение

#### вытаскивание данных ###
    cur.execute("SELECT * FROM superheroes")
    #one_line = cur.fetchone()
    #print(one_line)

    ful_fetch = cur.fetchall()
    for record in ful_fetch:
       print(record)
       print(record[1])




    conn.commit() # комит не закрывает соединение

    cur.close()# закрытие курсора
    conn.close() # закрытие подключения




    with psycopg2.connect( host="localhost", database="person", user="postgres", password="12345678") as conn: # Более правильный способ обращения к БД
      with conn.cursor() as cursor:
         cursor.execute(R"""
            DROP TABLE IF EXISTS mumu;
            CREATE TABLE mumu (hero_id serial PRIMARY KEY, hero_name varchar, strength int);
            INSERT INTO mumu (hero_name, strength) VALUES (%s, %s)
            
         """,("Supermam", 100))
      conn.commit()

   # with psycopg2.connect(**db_cred) as conn:
   #     with conn.cursor() as cursor:
   #         cursor.execute(R""" 
#
   #         """)

if __name__ == "__main__":
    main()

    #CREATE USER davide WITH PASSWORD 'jw8s0F4';
