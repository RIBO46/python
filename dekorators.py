#Декоратор функции
#func - функция, которую будем оборачивать
#То есть именно для функции func мы хотим дополнить поведение, не трогая ее код
def decorator(func): # В функцию decorator передается ссылка на функцию get_count
# wrapper - это как раз функция обертка, в ней мы добавляем нужные действия
# и вызываем функцию func
    def wrapper(*args, **kwargs):
        print ("Старт")
        sum = func(*args, **kwargs) #вызов функции get_count и присвоение значения в переменную sum
        print(sum)
        print ("Конец")
        return sum
    # Вызываем функцию-обертку, чтобы не вызывать ее сразу
    # а сделать потом в коде
    return wrapper # возвращаем ссылку на объект функции wrapper

#теперь применяем декоратор к нашей функции get_count
@decorator
def get_count(count1: int = 1, count2: int = 1):
    return count1 + count2

# теперь вызываем просто саму функцию
a = get_count(3, 10)
#print(a) 
