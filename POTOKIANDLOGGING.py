import threading, time
import logging, logging.handlers

def print_numbers():
    for i in range(10):
        logging.debug(i)
        print(i)

def print_letters():
    for letter in 'abcdefghij':
        #time.sleep(1)
        print(letter)
        logging.debug(letter) # логируем в файл вывод из переменной letter

if __name__ == '__main__':

    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')




    log = logging.getLogger()
    log.setLevel(logging.DEBUG)
    a=logging.FileHandler('log')
    a.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
    log.addHandler(a)
    #  В данном коде происходит настройка и использование модуля logging для записи логов в файл.    
    #  
    #  1. Сначала создается объект логгера с помощью вызова функции getLogger() из модуля logging.
    #  2. Затем устанавливается уровень логирования с помощью метода setLevel(). В данном случае установлен 
уровень DEBUG, что означает, что будут записываться все сообщения лога, включая сообщения с уровнями DEBUG, INFO, 
WARNING, ERROR и CRITICAL.
    #  3. Создается объект FileHandler, который будет использоваться для записи логов в файл с именем 'log'. 
    #  4. Устанавливается форматирование сообщений лога с помощью метода setFormatter(). В данном случае 
используется формат '%(message)s', что означает, что в лог будет записываться только текст сообщения без 
дополнительной информации.
    #  5. Объект FileHandler добавляется в список обработчиков логгера с помощью метода addHandler(). Таким 
образом, все сообщения лога будут записываться и в консоль, и в файл 'log'.


    # Создаем два потока
    thread1 = threading.Thread(target=print_numbers)
    thread2 = threading.Thread(target=print_letters)

    # Запускаем потоки
    thread1.start()
    thread2.start()

    # Ожидаем завершения работы потоков
    thread1.join()
    thread2.join()

    print('Программа завершена')
