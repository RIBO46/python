import requests

def main():

    sesion = requests.Session() #открываем сессию 

    params = {
        "param1": "key1",
        "param2": "key2"
    }

    headers = {
    'Accept': '*/*',
    "Authorization": "Bearer: FgmFSDMGLlgwM/KNoRJlebdD9CK5",
    'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    }

    cookies = {
    'JSESSIONID': 'sfsdfsdf',
    'seraph.rememberme.cookie': '1sdfdsfsdfds',
    'atlassian.xsrf.token': 'Bdsfdsfdsf',
    'mywork.tab.tasks': 'false',
    'SRVNAME': 'sfdsdf2',
    }

    payload = {
    "id": 123,
    "name": "Vasya"
    }



    base = "https://httpbin.org"
    response = requests.get(base + "/get", params=params) # передача реквеста в переменную response c query параметрами
    response = requests.get(base + "/get", headers=headers) # передача реквеста в переменную response c заголовками
    response = requests.get(base + "/get", cookies=cookies) # передача реквеста в переменную response c cookies
    response = requests.post(base + "/post", data=payload) # передача реквеста POST в переменную response c телом (body)
    response = sesion.post(base + "/post", data=payload) # все тоже самое только через сессию

    #print(response) # прсто получить репспонс
    #print(response.status_code) # получить код ответа
    #print(response.text) # получить текст ответа 
    print(response.json()) # получить текст ответа 

if __name__ == '__main__':
    main()