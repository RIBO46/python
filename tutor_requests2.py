# This code sample uses the 'requests' library:
# http://docs.python-requests.org
import requests

url = "https://yandex.ru"

query = {
   'key': 'APIKey',
   'token': 'APIToken'
}

response = requests.request(
   "GET",
   url,
   #params=query
   timeout=0.00001
)
print(response)