import paramiko

def main():

   with paramiko.SSHClient() as ssh_client:     

        ssh_client.load_system_host_keys("/Users/20111720/.ssh/known_hosts") # читаем файл известных хосов 
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(hostname="10.255.16.66", port=22, username="avmoskalev", password="123!") # тут передаем 4 обязательных парраметра      
        while True:
            cmd = input("$> ")
            if cmd == "exit": break
            stdin, stdout, stderr = ssh_client.exec_command(cmd)
            print(stdout.read().decode())

if __name__ == "__main__":
    main() 